### How to setup
1) Create a database in postgres for gitea

2) Check if you can connect to the database from the gitea container. 
   If not edit pg_hba.conf and postgres.conf

3) Create admin user from inside container
   `gitea admin create-user --admin`
