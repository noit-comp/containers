# Drupal News LDAP

## Building and running the drupal image for the first time

You should do the following:
1. Proceed with the standard Drupal installation. 
2. Install the LDAP and REST modules and activate them.
    - LDAP modules:
      - https://www.drupal.org/project/ldap/releases/8.x-3.0-beta7
      - https://www.drupal.org/project/externalauth/releases/8.x-1.3
      - https://www.drupal.org/project/authorization/releases/8.x-1.0-beta3
    - REST modules:
      - https://www.drupal.org/project/restui/releases/8.x-1.18
      - https://www.drupal.org/project/jsonapi/releases/8.x-2.4
      - https://www.drupal.org/docs/8/core/modules/rest   -> Already installed. Has to be activated.
3. Import the drupal-news-config.tar.gz.
4. Go to Administration -> Configuration -> People -> LDAP Servers. Edit fmi ldap by adding the real bind password.
5. Go to Administration -> Configuration -> People -> Account settings and disable user registration from visitors.


## Configuring CORS

**Creating or modifying any files should be done trough the container itself. Changing files directly from the named volume location may result in breaking the volume.**

CORS is can be configured in services.yml file, which is located in 'sites/default'. If the file doesn't exist, it should be created by copying the needed CORS section from default.services.yml in the same directory.

After every change in the configuration, the drupal cache should be deleted. This theoretically can be done in two ways:
1. Using drush, the CLI for drupal. It is not installed in the current drupal-ldap docker image.
2. Entering the drupal control panel (site) as an administrator in deleting the cache by pressing a button in Administration -> Configuration -> Development. This method can be impossible, because of the wrong configuration. Be sure to add the URL from which you want to delete the cache in 'allowedOrigins', before trying to login/register. If trying to login/register first, drupal will decide to not authenticate the user and will store that decision in the cache. Even if the URL is added then, if the cache is not deleted, it still not authenticate the user. This can be fixed by adding a new URL, for example, localhost with unique port, that is not used for login in up to now (not tested), or by going in the drupal code and removing the section that checks if the URL is allowed by CORS. This part of the code is located in '/var/www/html/vendor/asm89/stack-cors/src/Asm89/Stack/Cors.php'. After that, the cache can be easily deleted, following the steps above. Be sure to undo any changes in the drupal code after deleting the cache.
