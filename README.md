# Containers

Repo for all docker-compose and other container files

In production commented parts of `docker-compose.yml` files should be exported
with values in files named `docker-compose.prod.yml`. Then containers should be
started with
 
> docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d

which might be aliased to `dcup`.
